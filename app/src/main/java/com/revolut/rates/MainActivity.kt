package com.revolut.rates

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.revolut.rates.adapters.CurreciesAdapter
import com.revolut.rates.entitys.CountryCurrency
import com.revolut.rates.httpclient.MyHttpClient


class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: CurreciesAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var isRunning = false
    private var thread: Thread? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        isRunning = true

        supportActionBar?.hide()

        viewManager = LinearLayoutManager(this)

        viewAdapter = CurreciesAdapter(this)

        recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter
        }
    }


    override fun onResume() {
        super.onResume()

        startLoadingOnThread()

        isRunning = true
    }

    override fun onStop() {
        super.onStop()

        isRunning = false

        thread = null;
    }

    override fun onDestroy() {
        super.onDestroy()

        if (thread != null) {
            thread = null
        }

        isRunning = false
    }

    fun startLoadingOnThread() {

        thread = Thread {

            val ratesData = MyHttpClient.makeRequest()

            val currentRates = ratesData.getJSONObject("rates")

            val rateKeys = currentRates.keys()

            val countrysRateHashMap = HashMap<String, CountryCurrency>()

            var i = 0

            for (key in rateKeys) {

                val rate = currentRates.getDouble(key)

                val resId = this.resources.getIdentifier(
                    key.toLowerCase(),
                    "drawable",
                    this.packageName
                )

                var largeIcon = BitmapFactory.decodeResource(getResources(), resId);

                largeIcon = ImageTool().resizeImage(largeIcon, 120);

                if (largeIcon == null) {
                    val w = 50
                    val h = 50

                    val conf = Bitmap.Config.ARGB_8888 /// see other conf types

                    largeIcon = Bitmap.createBitmap(w, h, conf)
                }

                val countryCurrency = CountryCurrency(largeIcon, rate, key, key)

                countrysRateHashMap.set(key, countryCurrency)

                i++
            }

            runOnUiThread {

                viewAdapter.addAll(countrysRateHashMap)

            }

            try {
                Thread.sleep(1000)
            } catch (e: Exception) {
                return@Thread
            }

            while (isRunning) {

                if (this.isFinishing == false) {

                    val ratesData = MyHttpClient.makeRequest()

                    val currentRates = ratesData.getJSONObject("rates")

                    val rateKeys = currentRates.keys()

                    var i = 0

                    for (key in rateKeys) {

                        val rate = currentRates.getDouble(key)

                        countrysRateHashMap.get(key)?.countryRate = rate

                        i++
                    }

                    runOnUiThread {

                        viewAdapter.updateAll(countrysRateHashMap)

                    }

                    try {
                        Thread.sleep(1000)
                    } catch (e: Exception) {
                        return@Thread
                    }
                }
            }
        }

        thread!!.start()
    }
}
