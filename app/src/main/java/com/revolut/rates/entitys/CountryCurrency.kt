package com.revolut.rates.entitys

import android.graphics.Bitmap

class CountryCurrency(
    public val countryIcon: Bitmap,
    public var countryRate: Double,
    public val countryCode: String,
    public val countryName: String
) {


}
