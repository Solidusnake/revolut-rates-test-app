package com.revolut.rates.adapters;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.revolut.rates.ImageTool;
import com.revolut.rates.R;
import com.revolut.rates.entitys.CountryCurrency;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CurreciesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CountryCurrency> countryCurrenciesList = new ArrayList<CountryCurrency>();
    private Double theSumm = 0.0;
    private RecyclerView recyclerView = null;
    private int inputRowIndex = -1;

    public CurreciesAdapter() {
        inputRowIndex = -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.currencies_row_layout, parent, false);
        return new CurrencyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CurrencyViewHolder currencyViewHolder = (CurrencyViewHolder) holder;
        currencyViewHolder.setIsRecyclable(false);
        Double currencyRate = countryCurrenciesList.get(position).getCountryRate();

        if(inputRowIndex!=-1&&position==0){
            currencyViewHolder.currencyValue.setText(Double.toString( theSumm));
        }else{
            currencyViewHolder.currencyValue.setText(Double.toString(currencyRate * theSumm));
        }

        currencyViewHolder.currencyValue.setTag(position);
        currencyViewHolder.currencyImage.setImageBitmap(new ImageTool().getRoundedCornerImage(countryCurrenciesList.get(position).getCountryIcon(), 50));

        currencyViewHolder.currencyCode.setText(countryCurrenciesList.get(position).getCountryCode());
        currencyViewHolder.currencyName.setText(countryCurrenciesList.get(position).getCountryName());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        this.recyclerView = recyclerView;
    }

    @Override
    public int getItemCount() {
        return countryCurrenciesList.size();
    }

    // region StringList Helpers
    public CountryCurrency get(int position) {
        return countryCurrenciesList.get(position);
    }

    public void addAll(List<CountryCurrency> items) {

        for (CountryCurrency item : items) {
            countryCurrenciesList.add(item);
        }
        notifyDataSetChanged();
    }

    public void updateAll(List<CountryCurrency> items) {

        int i = 0;
        for (CountryCurrency item : items) {
            countryCurrenciesList.get(i).setCountryRate(item.getCountryRate());
            i++;
        }
    }

    public void addAll(CountryCurrency[] items) {
        addAll(Arrays.asList(items));
    }

    private class CurrencyViewHolder extends RecyclerView.ViewHolder {

        android.widget.ImageView currencyImage = null;
        android.widget.AutoCompleteTextView currencyValue = null;
        android.widget.TextView currencyCode = null;
        android.widget.TextView currencyName = null;

        public CurrencyViewHolder(View view) {
            super(view);

            currencyImage = (ImageView) view.findViewById(R.id.simpleImageView);
            currencyValue = (AutoCompleteTextView) view.findViewById(R.id.currency_textview);

            currencyCode = (TextView) view.findViewById(R.id.current_code);
            currencyName = (TextView) view.findViewById(R.id.current_name);

            final Handler handler = new Handler();

            currencyValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {

                    if (hasFocus) {

                        handler.removeCallbacksAndMessages(null);
                    }
                }
            });

            currencyValue.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (currencyValue.hasFocus()) {
                        String enteredSumm = currencyValue.getText().toString();

                        try {
                            theSumm = Double.parseDouble(enteredSumm);
                        } catch (NumberFormatException e) {
                            theSumm = 0.0;
                        }

                        handler.removeCallbacksAndMessages(null);

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                inputRowIndex = Integer.parseInt(currencyValue.getTag().toString());

                                notifyDataSetChanged();

                                LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();

                                CountryCurrency countryCurrency = countryCurrenciesList.get(inputRowIndex);

                                countryCurrenciesList.remove(inputRowIndex);

                                countryCurrenciesList.add(0, countryCurrency);

                                llm.scrollToPosition(0);

                            }
                        }, 2000);
                    }
                }
            });
        }
    }
}