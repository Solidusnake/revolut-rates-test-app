package com.revolut.rates.adapters

import android.content.Context
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.revolut.rates.ImageTool
import com.revolut.rates.R
import com.revolut.rates.entitys.CountryCurrency
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.*

class CurreciesAdapter (val context: Context) : RecyclerView.Adapter<ViewHolder>() {
    private var currenciesRateHashMap =
        HashMap<String, CountryCurrency>()
    private var currenciesKeysArray = arrayOfNulls<String>(0)
    private var currencyCountryHashMap = HashMap<String, String>()
    private var theSumm = 0.0
    private var inputSum = 0.0
    private var recyclerView: RecyclerView? = null
    private var inputRowIndex = -1

    init {
        inputRowIndex = -1
        val keys: Set<String?> = currenciesRateHashMap.keys
        currenciesKeysArray = keys.toTypedArray()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        try {
            val inputStream = context.assets.open("countries.json")

            val baos = ByteArrayOutputStream()

            inputStream.use { it.copyTo(baos) }

            val countriesString = baos.toString()

            val countriesJsonObject2 = JSONObject(countriesString)

            val countriesJsonObject3 = countriesJsonObject2.getJSONObject("countries")

            val countriesJsonArray = countriesJsonObject3.getJSONArray("country")

            for (x in 0..countriesJsonArray.length()) {
                val jsonObject = countriesJsonArray.getJSONObject(x)

                currencyCountryHashMap.set(jsonObject.getString("currencyCode"), jsonObject.getString("countryName"))
            }

        } catch (e: Exception) {

        }

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.currencies_row_layout, parent, false)
        return CurrencyViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currencyViewHolder = holder as CurrencyViewHolder
        currencyViewHolder.setIsRecyclable(false)
        val currencyRate =
            currenciesRateHashMap[currenciesKeysArray[position]]!!.countryRate

        if(position != 0||inputRowIndex==-1&&position==0){

            var summText = java.lang.Double.toString(currencyRate * theSumm)

            val indexOfSpot = summText.indexOf(".")

            val decimalAmount = summText.length - indexOfSpot

            if(6<decimalAmount){
                summText = summText.substring(0, indexOfSpot+5)
            }

            currencyViewHolder.currencyValue!!.setText(summText)
        }else{
            currencyViewHolder.currencyValue!!.setText(java.lang.Double.toString(inputSum))
        }

        currencyViewHolder.currencyValue!!.tag = position
        currencyViewHolder.currencyImage!!.setImageBitmap(
            ImageTool().getRoundedCornerImage(
                currenciesRateHashMap[currenciesKeysArray[position]]!!.countryIcon,
                50
            )
        )
        currencyViewHolder.currencyCode!!.text =
            currenciesRateHashMap[currenciesKeysArray[position]]!!.countryCode
        currencyViewHolder.currencyName!!.text = currencyCountryHashMap.get(currenciesKeysArray[position])
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun getItemCount(): Int {
        return currenciesRateHashMap.size
    }

    fun addAll(items: HashMap<String, CountryCurrency>) {
        currenciesRateHashMap = items
        val keys: Set<String?> = currenciesRateHashMap.keys
        currenciesKeysArray = keys.toTypedArray()
        notifyDataSetChanged()

        inputRowIndex = -1

        theSumm = 0.0
    }

    fun updateAll(items: HashMap<String, CountryCurrency>) {
        for ((key, value) in items) {
            currenciesRateHashMap[key] = value
        }
    }

    private inner class CurrencyViewHolder(view: View) : ViewHolder(view) {
        var currencyImage: ImageView? = null
        var currencyValue: AutoCompleteTextView? = null
        var currencyCode: TextView? = null
        var currencyName: TextView? = null

        init {
            currencyImage =
                view.findViewById<View>(R.id.simpleImageView) as ImageView
            currencyValue =
                view.findViewById<View>(R.id.currency_textview) as AutoCompleteTextView
            currencyCode = view.findViewById<View>(R.id.current_code) as TextView
            currencyName = view.findViewById<View>(R.id.current_name) as TextView
            val handler = Handler()
            currencyValue!!.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    handler.removeCallbacksAndMessages(null)
                }
            }
            currencyValue!!.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                }

                override fun afterTextChanged(s: Editable) {
                    if (currencyValue!!.hasFocus()) {
                        val enteredSumm = currencyValue!!.text.toString()
                        inputSum = try {
                            enteredSumm.toDouble()
                        } catch (e: NumberFormatException) {
                            0.0
                        }

                        val divider = currenciesRateHashMap.get(currenciesKeysArray[currencyValue!!.tag.toString().toInt()])!!.countryRate

                        theSumm = inputSum / divider

                        handler.removeCallbacksAndMessages(null)
                        handler.postDelayed({
                            inputRowIndex = currencyValue!!.tag.toString().toInt()
                            val llm =
                                recyclerView!!.layoutManager as LinearLayoutManager?
                            val entry = currenciesKeysArray[0]
                            currenciesKeysArray[0] = currenciesKeysArray[inputRowIndex]
                            currenciesKeysArray[inputRowIndex] = entry
                            llm!!.scrollToPosition(0)
                            notifyDataSetChanged()
                        }, 1000)
                    }
                }
            })
        }
    }

    init {
        inputRowIndex = -1
        val keys: Set<String?> = currenciesRateHashMap.keys
        currenciesKeysArray = keys.toTypedArray()
    }
}