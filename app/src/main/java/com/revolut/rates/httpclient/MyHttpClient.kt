package com.revolut.rates.httpclient

import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class MyHttpClient {
    companion object {
        fun makeRequest(): JSONObject {

            val url =
                URL("https://hiring.revolut.codes/api/android/latest?base=EUR")

            val urlConnection: HttpURLConnection = url.openConnection() as HttpURLConnection

            val content = StringBuilder()
            try {
                val inputStream: InputStream = BufferedInputStream(urlConnection.getInputStream())

                val reader = BufferedReader(inputStream.reader())

                try {
                    var line = reader.readLine()
                    while (line != null) {
                        content.append(line)
                        line = reader.readLine()
                    }
                } finally {
                    reader.close()
                }

            } finally {
                urlConnection.disconnect()
            }

            val jsonObject = JSONObject(content.toString())

            return jsonObject
        }
    }

}